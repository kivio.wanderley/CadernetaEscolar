package com.example.ekivuu.cadernetaescolar.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.example.ekivuu.cadernetaescolar.model.Materia;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EKivuu on 05/05/2017.
 */

public class MateriaDAO extends SQLiteOpenHelper {

    private final static String MATERIAS_TB = "Materias";
    private final static String MATERIAS_TB_TITULO = "titulo";
    private final static String MATERIAS_TB_DESCRICAO = "descricao";
    private int lastVersion = -1;

    public MateriaDAO(Context context) {
        super(context, "CadernetaEscolar", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlCreateTable =
                "CREATE TABLE "+ MATERIAS_TB +" (" +
                        "id INTEGER PRIMARY KEY," +
                        MATERIAS_TB_TITULO+" TEXT NOT NULL," +
                        MATERIAS_TB_DESCRICAO+" TEXT NOT NULL" +
                        ")";
        db.execSQL(sqlCreateTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 0:
                String sqlCreateTable =
                    "DROP TABLE IF EXISTS "+MATERIAS_TB;

                db.execSQL(sqlCreateTable);
                onCreate(db);
                break;
            case 1:
                break;
            default: { break; }
        }
    }

    @NonNull
    private ContentValues getContentvalues(Materia materia) {
        ContentValues contentValues = new ContentValues();

        contentValues.put(MATERIAS_TB_TITULO, materia.getTitulo());
        contentValues.put(MATERIAS_TB_DESCRICAO, materia.getDescricao());

        return contentValues;
    }

    public void create(Materia materia) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = getContentvalues(materia);
        db.insert(MATERIAS_TB, null, contentValues);
    }

    public void delete(Long id) {
        SQLiteDatabase db = getWritableDatabase();
        String[] params = { id.toString() };
        db.delete(MATERIAS_TB, "id = ?", params);
    }

    public void update(Materia materia) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = getContentvalues(materia);
        String[] params = { materia.getId().toString() };
        db.update(MATERIAS_TB, contentValues, "id = ?", params);
    }

    public Materia getById(Long id) {
        SQLiteDatabase db = getReadableDatabase();
        String sqlGetById =
                "SELECT * FROM "+MATERIAS_TB+" WHERE id = "+id+";";

        Cursor cursor = db.rawQuery(sqlGetById, null);

        List<Materia> materias = new ArrayList<Materia>();

        cursor.moveToNext();
        Materia materia = new Materia();
        materia.setId(cursor.getLong(cursor.getColumnIndex("id")));
        materia.setTitulo(cursor.getString(cursor.getColumnIndex(MATERIAS_TB_TITULO)));
        materia.setDescricao(cursor.getString(cursor.getColumnIndex(MATERIAS_TB_DESCRICAO)));

        cursor.close();

        return materia;
    }

    public List<Materia> getAll() {
        SQLiteDatabase db = getReadableDatabase();
        String sqlGetById =
                "SELECT * FROM "+MATERIAS_TB+";";

        Cursor cursor = db.rawQuery(sqlGetById, null);

        List<Materia> materias = new ArrayList<Materia>();

        while (cursor.moveToNext()) {
            materias.add(new Materia(
                    cursor.getLong(cursor.getColumnIndex("id")),
                    cursor.getString(cursor.getColumnIndex(MATERIAS_TB_TITULO)),
                    cursor.getString(cursor.getColumnIndex(MATERIAS_TB_DESCRICAO)))
            );
        }

        cursor.close();

        return materias;
    }
}
