package com.example.ekivuu.cadernetaescolar.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ekivuu.cadernetaescolar.R;
import com.example.ekivuu.cadernetaescolar.model.Materia;

import java.util.List;

/**
 * Created by EKivuu on 05/05/2017.
 */

public class MateriaListAdapter extends ArrayAdapter<Materia> {

    private final List<Materia> list;
    private LayoutInflater layoutInflater;
    private Context c;

    public MateriaListAdapter(Context c, List<Materia> list) {
        super(c, R.layout.materia_list_item, list);

        this.c = c;
        this.list = list;
        this.layoutInflater = LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Materia getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return this.list.get(i).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LinearLayout listLay = (LinearLayout)layoutInflater.inflate(R.layout.materia_list_item, parent, false);

        TextView tituloView = (TextView)listLay.findViewById(R.id.materia_list_item_titulo);

        Materia item = list.get(position);

        tituloView.setText((String) item.getTitulo());

        listLay.setTag(position);

        return listLay;
    }
}
