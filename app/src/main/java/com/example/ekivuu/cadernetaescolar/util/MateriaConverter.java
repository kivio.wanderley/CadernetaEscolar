package com.example.ekivuu.cadernetaescolar.util;

import com.example.ekivuu.cadernetaescolar.model.Materia;

import org.json.JSONException;
import org.json.JSONStringer;

import java.util.List;

/**
 * Created by EKivuu on 05/05/2017.
 */

public class MateriaConverter {
    public String converterJson(List<Materia> materias) {
        JSONStringer js = new JSONStringer();

        try {
            js.object().key("list").array().object().key("materia").array();

            for (Materia materia : materias) {
                js.object();
                js.key("titulo").value(materia.getTitulo());
                js.key("descricao").value(materia.getDescricao());
                js.endObject();
            }

            js.endArray().endObject().endArray().endObject();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return js.toString();
    }
}
