package com.example.ekivuu.cadernetaescolar;

import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by EKivuu on 05/05/2017.
 */

public class WebClient {

    public String post(String js) {
        String response = null;

        try {
            URL url = new URL("http://192.168.25.9:5000");

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");

            connection.setDoOutput(true);

            PrintStream output = new PrintStream(connection.getOutputStream());

            output.println(js);

            connection.connect();

            Scanner scanner = new Scanner(connection.getInputStream());

            response =  scanner.next();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }

}
