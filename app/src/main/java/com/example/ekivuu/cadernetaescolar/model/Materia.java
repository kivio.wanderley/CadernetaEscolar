package com.example.ekivuu.cadernetaescolar.model;

import java.io.Serializable;

/**
 * Created by EKivuu on 05/05/2017.
 */

public class Materia implements Serializable {

    private Long id;
    private String titulo;
    private String descricao;

    public Materia() {
    }

    public Materia(Long id, String titulo, String descricao) {
        this.id = id;
        this.titulo = titulo;
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return this.titulo.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
