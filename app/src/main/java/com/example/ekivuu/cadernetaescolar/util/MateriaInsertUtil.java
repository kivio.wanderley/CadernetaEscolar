package com.example.ekivuu.cadernetaescolar.util;

import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.example.ekivuu.cadernetaescolar.R;
import com.example.ekivuu.cadernetaescolar.model.Materia;

/**
 * Created by EKivuu on 05/05/2017.
 */

public class MateriaInsertUtil {

    private EditText titulo;
    private EditText descricao;
    private Materia materia;

    public MateriaInsertUtil(AppCompatActivity appCompatActivity) {
        titulo = (EditText) appCompatActivity.findViewById(R.id.titulo_id);
        descricao = (EditText) appCompatActivity.findViewById(R.id.descricao_id);
        materia = new Materia();
    }

    public Materia buildForInsert() throws Exception {
        if (this.titulo.getText().toString().isEmpty())
            throw new Exception("Campo TITULO obrigatório!");

        if (this.descricao.getText().toString().isEmpty())
            throw new Exception("Campo DESCRICAO obrigatório!");

        String titulo_str = this.titulo.getText().toString();
        String descricao_str = this.descricao.getText().toString();

        this.materia.setTitulo(titulo_str);
        this.materia.setDescricao(descricao_str);

        return this.materia;
    }
}
