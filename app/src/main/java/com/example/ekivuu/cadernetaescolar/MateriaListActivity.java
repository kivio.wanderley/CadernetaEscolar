package com.example.ekivuu.cadernetaescolar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ekivuu.cadernetaescolar.dao.MateriaDAO;
import com.example.ekivuu.cadernetaescolar.model.Materia;
import com.example.ekivuu.cadernetaescolar.util.MateriaConverter;
import com.example.ekivuu.cadernetaescolar.util.MateriaListAdapter;

import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Scanner;

public class MateriaListActivity extends AppCompatActivity {

    private List<Materia> list;
    private ListView caderneta_lista;
    private Button add_btt;
    private ImageButton send_btt;
    private MateriaDAO materiaDAO;

    public class SendMateriaTask extends AsyncTask {

        public SendMateriaTask(View.OnClickListener listener) {
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            materiaDAO = new MateriaDAO(MateriaListActivity.this);
            list = materiaDAO.getAll();

            MateriaConverter converter = new MateriaConverter();
            String json = converter.converterJson(list);

            WebClient webClient = new WebClient();
            return webClient.post(json);
        }

        @Override
        protected void onPostExecute(Object o) {
            String response = (String) o;
            Toast.makeText(getApplicationContext(), "Enviando notas "+response, Toast.LENGTH_SHORT).show();
            super.onPostExecute(o);
        }

        @Override
        protected void onPreExecute() {
            Toast.makeText(getApplicationContext(), "Aguarde... Enviando dados", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        buildList();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia_list);

        materiaDAO = new MateriaDAO(MateriaListActivity.this);

        caderneta_lista = (ListView) findViewById(R.id.caderneta_lista_id);
        add_btt = (Button) findViewById(R.id.caderneta_add_btt_id);
        send_btt = (ImageButton) findViewById(R.id.caderneta_send_btt_id);

        add_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MateriaListActivity.this, MateriaInsertActivity.class));
            }
        });

        send_btt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendMateriaTask task = (SendMateriaTask) new SendMateriaTask(this).execute();
            }

        });

        registerForContextMenu(caderneta_lista);
    }

    private void buildList() {
        MateriaDAO dao = new MateriaDAO(MateriaListActivity.this);
        this.list = dao.getAll();
        dao.close();

        MateriaListAdapter materiaListAdapter = new MateriaListAdapter(this, this.list);

        caderneta_lista.setAdapter(materiaListAdapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
//        super.onCreateContextMenu(menu, v, menuInfo);
        MenuItem removerMenuItem = menu.add("Remover");

        AdapterView.AdapterContextMenuInfo adapterMenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Materia item = (Materia) caderneta_lista.getItemAtPosition(adapterMenuInfo.position);
        final Materia materia = materiaDAO.getById((Long) item.getId());

        removerMenuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                materiaDAO.delete(materia.getId());

                buildList();

                Toast.makeText(MateriaListActivity.this, "Matéria "  + materia.getTitulo() + " removida!", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
