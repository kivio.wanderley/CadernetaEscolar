package com.example.ekivuu.cadernetaescolar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.ekivuu.cadernetaescolar.dao.MateriaDAO;
import com.example.ekivuu.cadernetaescolar.model.Materia;
import com.example.ekivuu.cadernetaescolar.util.MateriaInsertUtil;

public class MateriaInsertActivity extends AppCompatActivity {

    private EditText titulo;
    private EditText descricao;
    private Button adicionar;
    private MateriaInsertUtil materiaInsertUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_materia_insert);

        titulo = (EditText) findViewById(R.id.titulo_id);
        descricao = (EditText) findViewById(R.id.descricao_id);
        adicionar = (Button) findViewById(R.id.adicionar_btt_id);

        materiaInsertUtil = new MateriaInsertUtil(MateriaInsertActivity.this);

        adicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Materia materia = new Materia();

                try {
                    MateriaDAO dao = new MateriaDAO(MateriaInsertActivity.this);
                    materia =  materiaInsertUtil.buildForInsert();

                    if (materia.getId() != null) {
                        //TODO update
                    }
                    else {
                        dao.create(materia);
                    }

                    dao.close();
                    finish();

                    startActivity(new Intent(MateriaInsertActivity.this, MateriaListActivity.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
